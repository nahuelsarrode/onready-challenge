const Vehicle = require("./Vehicle");

class Motorbike extends Vehicle {
    size = ''; 

    constructor(name, model, size, price) {
        super(name, model, price); 
        this.size = size;
    }

    get size() {
        return this.size;
    }

    toString() {
        return `Marca: ${this.name} \// Modelo: ${this.model} // Cilindrada: ${this.size} // Precio ${super.priceFormatted}`;
    }
}

module.exports = Motorbike; 